/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijalmasilva.crudPersistencia;
/**
 *
 * @author dijalma silva
 */
public class Main {
    public static void main(String[] args) {
        CRUD crud = new CRUD();
        
        crud.cadastrar(new Pessoa("Dijalma Silva", 21));
        
        int idPessoa = 0;
        Pessoa p = crud.pesquisar(idPessoa);
        System.out.println(p);
        
        Pessoa pessoa = crud.pesquisar(idPessoa);
        pessoa.setNome("Manoel Dijalma");        
        crud.atualizar(pessoa);
        
        crud.remover(crud.pesquisar(idPessoa));
        
    }
}
